package car.data.controller.user;

import java.util.List;

import javax.ejb.Local;

import car.data.model.User;

@Local
public interface UserService {
 
	public User save(User user);
	public User getById(Long id);
	public List<User> getAll();
	
	
}
