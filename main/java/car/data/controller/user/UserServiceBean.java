package car.data.controller.user;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.html.parser.Entity;

import org.hibernate.annotations.Parent;

import car.data.model.Car;
import car.data.model.User;

@Stateless(name="UserService")
public class UserServiceBean implements UserService{

	@PersistenceContext
	EntityManager em;

	public User save(User user) {
		em.merge(user);
		return user;
	}

	
	public User getById(Long id) {
		return em.find(User.class, id);
	}
	
	public List<User> getAll(){
		List<User> users=em.createQuery("from User", User.class).getResultList();
		for (User user : users) {
			List<Car> cars = user.getCars();
			for (Car car : cars) {
				car.getId();
			}
		}
	return users;
	}
	
}
