package car.data.controller.user;

import car.data.model.User;
import car.data.view.UserMsg;

public class UserConverter {

	public UserMsg toMsg(User from) {
		UserMsg to = new UserMsg();
			to.setCars(from.getCars());
			to.setId(from.getId());
			to.setPassword(from.getPassword());
			to.setUserName(from.getUserName());
		return to;
	}
	
	public User toEntity(UserMsg from) {
		User to = new User();
			to.setCars(from.getCars());
			to.setId(from.getId());
			to.setPassword(from.getPassword());
			to.setUserName(from.getUserName());
		return to;
	}
}
