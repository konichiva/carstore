package car.data.controller.user;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import car.data.model.User;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("user")
public class UserResource {
	
	@Inject
	private UserService userService;
	
	
	
	@GET 
	public List<User> getAll(){
		return userService.getAll();
	}
	

}
