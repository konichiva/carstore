package car.data.controller.car;

import car.data.model.Car;
import car.data.view.CarMsg;

public class CarConnverter {

	public CarMsg toMsg (Car from) {
	CarMsg to = new CarMsg();
		to.setId(from.getId());
		to.setCarColor(from.getCarColor());
		to.setCarName(from.getCarName());
		to.setHorsePower(to.getHorsePower());
		to.setOwner(from.getOwner());
	return to;
	}
	
	public Car toEntity(CarMsg from) {
	Car to= new Car();
		to.setId(from.getId());
		to.setCarColor(from.getCarColor());
		to.setCarName(from.getCarName());
		to.setHorsePower(to.getHorsePower());
		to.setOwner(from.getOwner());
	return to;
	}
	
}
